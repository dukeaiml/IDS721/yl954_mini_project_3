import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class Yl954MiniProject3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create an S3 Bucket and add bucket properties like versioning and encryption
    const bucket = new s3.Bucket(this, 'Mini3', {
      bucketName: 'mini3',
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED,
    });

  }
}
